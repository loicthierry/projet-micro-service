# Pricing

## Functional

- Handle price over time
- Handle commands

## Technical

- Code in python
  - use of mqtt-paho library (<https://pypi.org/project/paho-mqtt/>)
- Dedicated database
- Documentation
  - API
  - Database model
