# Stock

## Functional requirements

- Handle stock of every items over time

## Technical

- Code in python
  - use of mqtt-paho library (<https://pypi.org/project/paho-mqtt/>)
- Dedicated database
- Documentation
  - API
  - Database model
  