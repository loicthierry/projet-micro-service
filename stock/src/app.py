import mariadb
import paho.mqtt.publish as mqtt_publish
import paho.mqtt.subscribe as mqtt_subscribe


config_db = {
    # Use the container name defined in the docker-compose file
    'host': 'stock_db',
    'port': 3306,
    'user': 'root',
    'password': 'root',
    'database': 'mydatabase'
}

config_mqtt = {
    'host': 'mqtt-broker'
}


# Use to make query to the database
def execute_query(query):
    # connection for MariaDB
    conn = mariadb.connect(**config_db)
    # create a connection cursor
    cur = conn.cursor() 
    # execute a SQL statement
    cur.execute(query)
    if cur.description:
        # serialize results into JSON
        row_headers = [x[0] for x in cur.description]
        rv = cur.fetchall()
        list_result = []
        for result in rv:
            list_result.append(dict(zip(row_headers, result)))
        return list_result
    else:
        conn.commit()
        return cur.lastrowid

# Publish the payload on the topic
def publish(topic, payload):
    mqtt_publish.single(topic, payload, hostname=config_mqtt['host'])

# Return the msg (this function is blocking)
def subscribe(topics): 
    return mqtt_subscribe.simple(topics, hostname=config_mqtt['host'])

if __name__ == '__main__':
    # TODO: make you're application using the MQTT broker (subscribe/publish)
    print("Hello world")
    # Here a simple exemple of a communication between the stock and pricing
    print("Sending...")
    publish("test", "hello world")
    print("sent")

