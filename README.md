# Micro-services architecture project

## Project

For this project you will need to build an application following the micro-services architecture.

To build this application you will need to form teams of 3 people, each of the person will build one service.

## Existing files

- docker-compose.yml
  - File to launch all the containers at once
  - launch with `docker-compose up --build`
- facturation
  - folder with the facturation service
  - the docker will launch `app.py`
- ihm
  - folder with the ui service
  - the docker will build and launch `app.ts`
- mosquitto
  - folder for the config files of the MQTT brocker
  - you should not modify this folder
- stock
  - folder with the stock service
  - the docker will build and launch `app.py`

## Architecture

The application will be split in 3 services :

- UI
- Stock management
- Pricing

We will be using the "Database-per-service" approach wich means that every service will have it's own database.

### MQTT

Every services will communicate with the MQTT protocol.

To do so you will need to create a protocol that allows the services to cummunicates with each others.
This protocol is free for you to do but needs to be documented as complete as possible.

Every services will need to follow the technical and functional requirements that will be listed below.

## Expectations

In this part I will be listing all the expectations of this project.

This project will need to be versioned by a git project on gitlab. (<https://gitlab.com/>)
You will need to invite me as a Maintainer: @loicthierry / loic.thierry@irit.fr
Every person will need to commit there work on a regular basis (small commit with understandable title).
(Be careful not to work on the main branch)

Every functionals and Technical requirements are to be met for a minimal system.
The application can be expanded as much as you want if you have time.

### UI

[See IHM Readme file](./ihm/README.md)

### Stock

[See Stock Readme file](./stock/README.md)

### Pricing

[See Facturation Readme file](./facturation/README.md)
