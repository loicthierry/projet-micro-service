console.log("Hello world")
import { connect } from "mqtt"
// import {  } from "inquirer"



var client  = connect('mqtt://mqtt-broker')

client.on('connect', function () {
  client.subscribe('test', function (err) {
    if (!err) {
      client.publish('test', 'Hello mqtt')
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  client.end()
})