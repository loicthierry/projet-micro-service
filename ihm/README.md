# UI

Functional requirements :

- client interface
  - Search items
    - name
    - price
    - in stock
    - ...
  - Item price
    - current
    - evolution over time
  - Item stock
  - create a command
    - Add items
    - See price
  - Confirm command
- Manager interface
  - Add stock
  - Create new item
    - Initial stock
    - Price

Technical requirements :

- Code in Typescript
- Use of the inquirer library (<https://www.npmjs.com/package/inquirer>)
